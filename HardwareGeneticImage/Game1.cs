﻿#region Using Statements
using System;
using System.Collections.Generic;
using System.Linq;
//using Microsoft.Xna.Framework.GamerServices;
using System.Drawing;
using System.IO;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
#endregion

namespace HardwareGeneticImage
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class Game1 : Game
    {
        GraphicsDeviceManager Graphics;
        SpriteBatch SpriteBatch;
        Texture2D Target;
        Texture2D[] Sources;
        GeneticImage GeneticImage;

        public Game1()
            : base()
        {
            Graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            SpriteBatch = new SpriteBatch(GraphicsDevice);
            GeneticImage = new GeneticImage(GraphicsDevice);
            Target = TextureHelper.LoadDynamic("Content/rob.png", GraphicsDevice); 
            Sources = new List<string>{
                    "rob.png"
                    }
                    .Select(s => TextureHelper.LoadDynamic("Content/" + s, GraphicsDevice))
                    .ToArray();
            Texture2D t = GeneticImage.EvolveSimilar(Target, Sources, 1, 1, 52, 72);
            using (FileStream f = new FileStream("output0.png", FileMode.OpenOrCreate))
                t.SaveAsPng(f, t.Width, t.Height);

            //t = GeneticImage.EvolveSimilar(Target, Sources, 400, 1000, 52, 72, false);
            //using (FileStream f = new FileStream("output1.png", FileMode.OpenOrCreate))
            //    t.SaveAsPng(f, t.Width, t.Height);

            // TODO: use this.Content to load your game content here
        }

        protected override void Update(GameTime gameTime)
        {
            Exit();
        }
    }
}
