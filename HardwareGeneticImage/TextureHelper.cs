﻿using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.InteropServices;
using System.Text;

namespace HardwareGeneticImage
{
    public static class TextureHelper
    {
        private static readonly Dictionary<string, Bitmap> _c = new Dictionary<string, Bitmap>();

        private static string GetCacheLocation(string url)
        {
            if (!Directory.Exists("cache"))
                Directory.CreateDirectory("cache");
            return "cache/" + HashHelper.GetMd5Sum(url) + ".png";
        }

        public static Texture2D LoadDynamic(Bitmap b, GraphicsDevice g)
        {
            // Initialize our custom texture (should be defined in the class)
            Texture2D customTexture = new Texture2D(g, b.Width, b.Height, false, SurfaceFormat.Color);

            // Lock the bitmap data
            BitmapData data = b.LockBits(new System.Drawing.Rectangle(0, 0, b.Width, b.Height), System.Drawing.Imaging.ImageLockMode.ReadOnly, b.PixelFormat);

            // calculate the byte size: for PixelFormat.Format32bppArgb (standard for GDI bitmaps) it's the height * stride
            int bufferSize = data.Height * data.Stride; // stride already incorporates 4 bytes per pixel

            // create buffer
            byte[] bytes = new byte[bufferSize];

            // copy bitmap data into buffer
            Marshal.Copy(data.Scan0, bytes, 0, bytes.Length);

            // copy our buffer to the texture
            customTexture.SetData(bytes);

            // unlock the bitmap data
            b.UnlockBits(data);

            return customTexture;
        }

        public static Texture2D LoadDynamic(string url, GraphicsDevice g)
        {
            if (!url.StartsWith("http"))
            {
                using (FileStream f = new FileStream(url, FileMode.Open))
                    return Texture2D.FromStream(g, f);
            }
            else
            {
                Texture2D o = null;
                try
                {
                    HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(url);
                    webRequest.AllowWriteStreamBuffering = true;
                    webRequest.UserAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:23.0) Gecko/20100101 Firefox/23.0";
                    WebResponse webResponse = webRequest.GetResponse();
                    using (Stream webStream = webResponse.GetResponseStream())
                        o = Texture2D.FromStream(g, webStream);

                    webResponse.Close();
                }
                catch
                {
                    return null;
                }

                return o;
            }
        }

        public static Bitmap DownloadBitmap(string url)
        {
            if (_c.ContainsKey(url))
                return _c[url];
            if (File.Exists(GetCacheLocation(url)))
                return Bitmap.FromFile(GetCacheLocation(url)) as Bitmap;
            WebClient w = new WebClient();
            byte[] bytes = w.DownloadData(url);

            Bitmap o = null;
            try
            {
                HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(url);
                webRequest.AllowWriteStreamBuffering = true;
                webRequest.UserAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:23.0) Gecko/20100101 Firefox/23.0";
                WebResponse webResponse = webRequest.GetResponse();
                using (Stream webStream = webResponse.GetResponseStream())
                {
                    using (Image tempImage = Image.FromStream(webStream))
                    {
                        const int maxSize = 1920 * 1080;
                        if (tempImage.Width * tempImage.Height > maxSize)
                            return null;
                        o = new Bitmap(tempImage);
                    }
                }
                webResponse.Close();
            }
            catch
            {
                return null;
            }

            o.Save(GetCacheLocation(url));
            return (_c[url] = o);
        }
    }
}
