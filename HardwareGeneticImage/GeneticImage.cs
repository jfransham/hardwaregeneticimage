﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HardwareGeneticImage
{
    public class GeneticImage
    {
        public const bool DEBUG = true;
        public readonly GraphicsDevice GraphicsDevice;
        public readonly SpriteBatch SpriteBatch;

        public GeneticImage(GraphicsDevice g)
        {
            GraphicsDevice = g;
            SpriteBatch = new SpriteBatch(g);
        }

        public Texture2D EvolveSimilar(Texture2D target, string[] sources, int children, int generations, int frameSize = 8)
        {
            return EvolveSimilar(target, sources.Select(s => TextureHelper.LoadDynamic(s, GraphicsDevice)).ToArray(), children, generations, frameSize);
        }

        public Texture2D EvolveProgressive(Texture2D target, Texture2D[] sources, int children, int frameSize = 8, int maxFrameSize = 256)
        {
            Random r = new Random();
            RenderTarget2D current = GetInitial(sources, frameSize, target.Width, target.Height, r, maxFrameSize);

            //double currentSimilarity = GetSimilarity(current, target);
            for (int x = 0; x < target.Width; x += frameSize)
            {
                for (int y = 0; y < target.Height; y += frameSize)
                {
                    RenderTarget2D best = null;
                    double currentDistance = double.PositiveInfinity;
                    for (int c = 0; c < children; c++)
                    {
                        RenderTarget2D v = GetVariationProgressive(current, sources, r, new Point(x, y), frameSize, maxFrameSize);
                        double dst = GetDistance(v, target);
                        if (dst < currentDistance)
                        {
                            if (best != null)
                                best.Dispose();

                            best = v;
                            currentDistance = dst;

                            if (DEBUG)
                            {
                                //GraphicsDevice.SetRenderTarget(null);
                                //SpriteBatch.Begin();
                                //SpriteBatch.Draw(best, new Vector2(0, current.Height));
                                //SpriteBatch.End();
                                //current.SaveAsPng(new FileStream("TEST" + r.Next() + ".png", FileMode.OpenOrCreate), current.Width, current.Height);
                            }
                        }
                        else
                        {
                            v.Dispose();
                        }
                    }

                    current.Dispose();
                    current = best;

                    if (DEBUG && ((x + y * target.Width) % 10 == 0))
                    {
                        //GraphicsDevice.SetRenderTarget(null);
                        //SpriteBatch.Begin();
                        //SpriteBatch.Draw(current, new Vector2());
                        //SpriteBatch.End();
                        using (FileStream f = new FileStream("TEST" + DateTime.Now.Ticks.ToString("X") + ".png", FileMode.OpenOrCreate))
                            current.SaveAsPng(f, current.Width, current.Height);
                    }
                }
            }

            return current;
        }

        public Texture2D EvolveSimilar(Texture2D target, Texture2D[] sources, int children, int generations, int frameSize = 8, int maxFrameSize = 256, bool useHsl = false)
        {
            Random r = new Random();
            RenderTarget2D current = GetInitial(sources, frameSize, target.Width, target.Height, r, maxFrameSize);
            string prefix = "TEST" + DateTime.Now.Ticks.ToString("X");
            
            //double currentSimilarity = GetSimilarity(current, target);
            for (int g = 0; g < generations; g++)
            {
                RenderTarget2D best = null;
                double currentDistance = double.PositiveInfinity;
                for (int c = 0; c < children; c++)
                {
                    RenderTarget2D v = GetVariation(current, sources, r, frameSize, maxSize: maxFrameSize);
                    double dst = GetDistance(v, target, useHsl);
                    if (dst < currentDistance)
                    {
                        if (best != null)
                            best.Dispose();

                        best = v;
                        currentDistance = dst;
                    }
                    else
                    {
                        v.Dispose();
                    }
                }

                current.Dispose();
                current = best;

                if (DEBUG && (g % 50 == 0))
                {
                    if (!Directory.Exists(prefix))
                        Directory.CreateDirectory(prefix);

                    using (FileStream f = new FileStream(prefix + "/" + g.ToString("X") + ".png", FileMode.OpenOrCreate))
                        current.SaveAsPng(f, current.Width, current.Height);
                }
            }

            return current;
        }

        private RenderTarget2D GetInitial(Texture2D[] sources, int frameSize, int w, int h, Random r, int maxSize = 256)
        {
            RenderTarget2D rnd = new RenderTarget2D(GraphicsDevice, w, h);
            for (int x = 0; x < rnd.Width; x += frameSize)
                for (int y = 0; y < rnd.Height; y += frameSize)
                {
                    DrawFrame(rnd, sources, new Point(x, y), frameSize, r, Color.White, maxSize);
                }
            return rnd;
        }

        private void DrawFrame(RenderTarget2D dest, Texture2D[] sources, Point d, int frameSize, Random r, int maxSize)
        {
            DrawFrame(dest, sources, d, frameSize, r, Color.White, maxSize);
        }

        private void DrawFrame(RenderTarget2D dest, Texture2D[] sources, Point d, int frameSize, Random r, Color c, int maxSize)
        {
            GraphicsDevice.SetRenderTarget(dest);
            SpriteBatch.Begin();
            int sourceI = r.Next(sources.Length);
            Texture2D source = sources[sourceI];
            int size = frameSize + r.Next(maxSize - frameSize);
            Rectangle rct = new Rectangle(
                r.Next(source.Width - size),
                r.Next(source.Height - size),
                size,
                size
                );
            
            SpriteBatch.Draw(source, new Rectangle(d.X, d.Y, frameSize, frameSize), rct, c);
            SpriteBatch.End();
        }

        private RenderTarget2D GetVariation(Texture2D old, Texture2D[] sources, Random r, int frameSize = 8, int numAdditions = 5, int maxSize = 256)
        {
            RenderTarget2D rnd = new RenderTarget2D(GraphicsDevice, old.Width, old.Height);
            GraphicsDevice.SetRenderTarget(rnd);
            SpriteBatch.Begin();
            SpriteBatch.Draw(old, new Vector2());
            SpriteBatch.End();
            for (int i = 0; i < numAdditions; i++)
                DrawFrame(rnd, sources,
                    GetRandomPoint(old.Width, old.Height, frameSize, r),
                    frameSize,
                    r,
                    Color.White,
                    maxSize
                    );

            return rnd;
        }

        private RenderTarget2D GetVariationProgressive(Texture2D old, Texture2D[] sources, Random r, Point p, int frameSize = 8, int numAdditions = 5, int maxSize = 256)
        {
            RenderTarget2D rnd = new RenderTarget2D(GraphicsDevice, old.Width, old.Height);
            GraphicsDevice.SetRenderTarget(rnd);
            SpriteBatch.Begin();
            SpriteBatch.Draw(old, new Vector2());
            SpriteBatch.End();
            for (int i = 0; i < numAdditions; i++)
                DrawFrame(rnd, sources,
                    p,
                    frameSize,
                    r,
                    Color.White,
                    maxSize
                    );

            return rnd;
        }

        private Point GetRandomPoint(int w, int h, int frameSize, Random r)
        {
            int fX = r.Next(w / frameSize) * frameSize;
            int fY = r.Next(h / frameSize) * frameSize;
            return new Point(fX, fY);
        }

        private double GetDistance(Texture2D a, Texture2D b, bool useHsv = true)
        {
            Color[] aSim = new Color[a.Width * a.Height],
                bSim = new Color[b.Width * b.Height];
            a.GetData<Color>(aSim);
            b.GetData<Color>(bSim);

            object lck = new object();
            double sim = 0;
            for (int i = 0; i < aSim.Length; i++)
            {
                sim += useHsv ?
                    GetHSLDistance(aSim[i], bSim[i]) :
                    GetDistance(aSim[i], bSim[i]);
            }

            return sim;
        }

        private long GetDistance(Color a, Color b)
        {
            long rDiff = (int)a.R - b.R,
                    gDiff = (int)a.G - b.G,
                    bDiff = (int)a.B - b.B;
            return rDiff * rDiff + gDiff * gDiff + bDiff * bDiff;
        }

        private double GetHSLDistance(Color a, Color b)
        {
            System.Drawing.Color dA = System.Drawing.Color.FromArgb(a.A, a.R, a.G, a.B),
                dB = System.Drawing.Color.FromArgb(b.A, b.R, b.G, b.B);
            float aH = dA.GetHue() / 360,
                aS = dA.GetSaturation(),
                aL = dA.GetBrightness(),
                bH = dB.GetHue() / 360,
                bS = dB.GetSaturation(),
                bL = dB.GetBrightness();

            float hDiff = 2 * (pmod((aH - bH) + 0.5f, 1) - 0.5f),
                sDiff = aS - bS,
                lDiff = aL - bL;

            return hDiff * hDiff + sDiff * sDiff + lDiff * lDiff;
        }

        private float pmod(float a, float n)
        {
            return a - (float)Math.Floor(a / n) * n;
        }
    }
}
